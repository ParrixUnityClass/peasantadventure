﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indoor : MonoBehaviour {
    [SerializeField] GameObject techo;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerStay(Collider ot)
    {
        if(ot.GetComponent<MineTird>() != null)
        {
            techo.SetActive(false);
        }
    }
    private void OnTriggerExit(Collider ot)
    {
        if (ot.GetComponent<MineTird>() != null)
        {
            techo.SetActive(true);
        }
    }
}
